# Hilda Nilsson

Hilda Nilsson, known as the "Angel Maker on Bruk Street," was a Swedish baby farmer and one of the most notorious figures in the history of this practice. She was convicted of murdering numerous infants entrusted to her care, and her case remains one of the darkest chapters in Swedish criminal history.

## Historical Context
Hilda Nilsson operated in the early 20th century in Helsingborg, Sweden. Baby farming was a common practice at the time, where women would take in infants for a fee, often from unwed mothers or destitute families who could not care for them. Nilsson, however, exploited this system for her own gain, leading to the deaths of several infants.

## The Crimes
Nilsson would take in infants and, instead of providing care, she would murder them shortly after receiving them. Her methods included drowning the infants in her washbasin or using other means to ensure their deaths. She would then dispose of the bodies to cover up her crimes. 

Her activities came to light in 1917 when suspicions arose due to the high number of children who had disappeared under her care. An investigation led to the discovery of the bodies of several infants, and Nilsson was arrested and charged with their murders.

During her trial, Nilsson confessed to the murders, and it was revealed that she had killed at least eight infants. She was found guilty and sentenced to death. However, before the sentence could be carried out, Nilsson committed suicide in her cell by hanging herself.

## Impact and Legacy
The case of Hilda Nilsson shocked Sweden and brought attention to the dark side of baby farming. It underscored the need for better regulation and oversight of child care practices. The public outcry over her crimes contributed to changes in laws and greater awareness of the vulnerabilities faced by infants and the poor.

Nilsson’s actions serve as a grim reminder of the potential for exploitation and cruelty in systems that lack proper safeguards and accountability. Her case also highlights the desperation of unwed mothers and poor families who were forced to resort to baby farmers in the absence of better options.

## Evidence

### Online Resources
- [Wikipedia: Hilda Nilsson](https://en.wikipedia.org/wiki/Hilda_Nilsson)