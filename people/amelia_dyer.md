# Amelia Dyer

Amelia Dyer is one of the most infamous figures in the history of baby farming. Operating in Victorian Britain, she was responsible for the deaths of numerous infants entrusted to her care. Her case shocked the nation and led to significant changes in the regulation of child welfare.

## Historical Context
Amelia Dyer began her career as a nurse and midwife before turning to baby farming, a practice where women would take in infants for a fee, often with the understanding that the child would be adopted or well-cared for. However, Dyer exploited this system for financial gain, neglecting and ultimately murdering many of the children in her care.

## The Crimes
Dyer's method involved advertising her services in newspapers, promising a safe and loving home for unwanted infants. Once in her care, the children were often starved, drugged with opiates to keep them quiet, and left to die. She would then dispose of the bodies, often in the River Thames. Her activities went largely unnoticed until 1896, when the body of a baby girl was found in the river, leading to her arrest.

During her trial, it was revealed that Dyer had been responsible for the deaths of an unknown number of infants, with estimates ranging from dozens to hundreds. She was found guilty of murder and was hanged on June 10, 1896.

## Impact and Legacy
The case of Amelia Dyer brought widespread attention to the horrors of baby farming and the lack of regulations protecting vulnerable children. Public outrage led to calls for reform, and subsequently, stricter laws were enacted to regulate adoption and childcare practices. The Infant Life Protection Act of 1897 was one of the legislative measures introduced to prevent such atrocities in the future.

Dyer's crimes also highlighted the desperate conditions faced by unwed mothers and poor families, who often had no choice but to resort to baby farmers. Her legacy is a grim reminder of the exploitation and neglect that can occur in the absence of adequate social and legal protections.

## Evidence

### Sources
1. **Books**:
   - "Amelia Dyer: Angel Maker: The Woman Who Murdered Babies for Money" by Alison Rattle and Allison Vale.
   - "The Baby Farmers: A Chilling Tale of Missing Children, Murder, and the Victorian Underworld" by Annie Cossins.

### Online Resources
- [Wikipedia: Amelia Dyer](https://en.wikipedia.org/wiki/Amelia_Dyer)

## Further Reading
For more detailed accounts and evidence, please refer to the provided sources and further reading sections.