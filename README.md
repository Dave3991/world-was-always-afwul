# Horrible Historical Events

Welcome to the Horrible Historical Events repository. This repository is dedicated to documenting and sharing stories about various tragic and dark events from history. Our goal is to provide well-researched information along with evidence to support these historical accounts.

## Table of Contents

### Events:
1. [Orphan Trains](events/orphan_trains.md)
2. [Pecunia Non Olet(money doesn't stink)](events/pecunia_non_olet.md)
3. [Baby Farming](events/baby_farming.md)
4. [Typhoid Mary](events/baby_farming.md)
5. [Radium Girls](events/radium_girls.md)
6. [Triangle Shirtwaist Factory Fire](events/triangle_shirtwaist_factory_fire.md)
7. [Techa River Contamination by the Mayak Complex](events/techa_river_contamination_by_the_mayak_complex.md)
8. [Mary (elephant)](events/mary_(elephant).md)

### People:
1. [Amelia Dyer](people/amelia_dyer.md)
2. [Hilda Nilsson](people/hilda_nilsson.md)