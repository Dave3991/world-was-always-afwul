# Triangle Shirtwaist Factory Fire

The Triangle Shirtwaist Factory fire was one of the deadliest industrial disasters in U.S. history. On March 25, 1911, a fire broke out at the Triangle Shirtwaist Factory in New York City, killing 146 garment workers. Most of the victims were young immigrant women.

## Historical Context
The Triangle Shirtwaist Factory occupied the top three floors of the Asch Building in Manhattan. The workers, primarily young immigrant women, worked long hours in poor conditions for low wages. The fire started in a rag bin, and the high levels of flammable materials and the lack of proper fire safety measures contributed to the rapid spread of the fire.

## Impact and Legacy
The factory's owners had locked the exit doors to prevent theft and unauthorized breaks, trapping many workers inside. Some workers jumped from the windows to escape the flames, leading to their deaths. The public outrage over the tragedy led to significant changes in labor laws and safety regulations.

The Triangle Shirtwaist Factory fire was a catalyst for the labor movement and resulted in the establishment of new workplace safety standards. It led to the creation of the Factory Investigating Commission and the passage of more stringent fire safety codes and labor laws.

## Evidence
1. **Books**:
   - "Triangle: The Fire That Changed America" by David Von Drehle.
   - "The Triangle Fire, Protocols Of Peace: And Industrial Democracy In Progressive Era New York" by Richard A. Greenwald.
2. **Academic Papers**:
   - Research articles on the impact of the fire on labor laws and workplace safety regulations.

### Online Resources
- [Wikipedia: Triangle Shirtwaist Factory Fire](https://en.wikipedia.org/wiki/Triangle_Shirtwaist_Factory_fire)
- [PBS: The Triangle Fire](https://www.pbs.org/wgbh/americanexperience/films/triangle/)
