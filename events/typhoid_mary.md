# Mary Mallon (Typhoid Mary)

Mary Mallon, commonly known as Typhoid Mary, was an Irish cook who became notorious as an asymptomatic carrier of typhoid fever. Born in 1869, she emigrated to the United States in 1883 and worked in various households as a cook. Mallon was the first person in the United States identified as an asymptomatic carrier of the pathogen associated with typhoid fever.

## Historical Context
From 1900 to 1907, Mary Mallon worked as a cook in New York City and Long Island. During this period, she infected 53 people with typhoid fever, three of whom died. Her role in spreading the disease was identified by George Soper, a sanitary engineer, who traced multiple outbreaks to her employment at various households.

## Impact and Legacy
Mallon was quarantined by public health authorities in 1907 but was released in 1910 under the condition that she would no longer work as a cook. However, she violated this agreement and resumed cooking, leading to further outbreaks. In 1915, she was placed in quarantine again, where she remained for the rest of her life.

Mary Mallon's case highlighted the importance of asymptomatic carriers in the spread of infectious diseases and led to changes in public health policies. Her story is a reminder of the ethical and legal challenges in balancing individual rights with public health safety.

## Evidence
1. **Books**:
   - "Typhoid Mary: Captive to the Public's Health" by Judith Walzer Leavitt.

### Online Resources
- [Wikipedia: Mary Mallon](https://en.wikipedia.org/wiki/Mary_Mallon)