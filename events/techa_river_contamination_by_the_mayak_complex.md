# Techa River Contamination by the Mayak Complex

The Techa River in Chelyabinsk, Russia, has been heavily contaminated due to the activities of the Mayak atomic complex. For decades, the Mayak facility released radioactive water directly into the river, leading to severe environmental and health consequences for the surrounding area.

## Historical Context
The Mayak complex, established in the late 1940s, was one of the Soviet Union's primary nuclear facilities. Between the late 1940s and early 1950s, radioactive waste from Mayak was routinely discharged into the Techa River, which served as a water source for nearby villages. This contamination exposed thousands of residents to high levels of radiation, causing long-term health issues and environmental degradation.

## Impact and Legacy
The contamination of the Techa River by the Mayak complex has had profound and lasting effects. The area is considered one of the most polluted places on Earth, with significantly elevated rates of cancer, genetic mutations, and other health problems among the local population. The Soviet government initially kept the extent of the contamination a secret, and it was only later that the full scale of the disaster became known.

Efforts to clean up and contain the radioactive pollution have been ongoing, but the legacy of the contamination persists. The incident has underscored the dangers of nuclear waste management and has led to increased scrutiny and regulation of nuclear facilities worldwide.

## Evidence
1. **Books**:
   - "Plutopia: Nuclear Families, Atomic Cities, and the Great Soviet and American Plutonium Disasters" by Kate Brown.

### Online Resources
- [ns.iaea.org](https://www-ns.iaea.org/downloads/rw/projects/emras-aquatic-techa.pdf)