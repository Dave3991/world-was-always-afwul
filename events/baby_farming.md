# Baby Farming

The term “baby farming” was common in late nineteenth and early twentieth-century cities but by 1920 or so most states had taken action against the commercial practices it suggested and the term was on the decline. It referred to placing-out infants for money as well as to their sale for profit. Many clients were unwed mothers, prostitutes, and destitute or deserted wives who needed help with their children while they worked for wages.

## Historical Context
Although most baby farming amounted to what we now call family day care, it developed a terrible reputation when exposés uncovered horrific abuses and horrible death traps. Stories about baby farming in newspapers and magazines were reported in lurid detail that called upon crude gender, racial, ethnic, and class stereotypes. These scandals helped to mobilize political support for child welfare regulation, including minimum standards such as state licensing, certification of child-placers, and investigation of foster homes.

Baby farming was condemned for being lethal, profitable, and at odds with child welfare. At a time when public health reformers documented astronomical rates of infant mortality in poor, congested urban communities and congregate institutions, it came as no surprise that babies consigned to farms often died there, victims of epidemic disease and unsanitary conditions.

## Impact and Legacy
The entrepreneurial side of baby farming was also used to vilify extreme forms of commercial adoption, in which babies were bought and sold like other commodities. Baby farmers sometimes profited on both ends of the adoption transaction, first extracting fees from desperate birth mothers and then demanding large sums from adopters. A survey by the Chicago Juvenile Protective Association reported that children were sold for up to $100 in the 1910s, with a percentage down and the balance in installments. No questions were asked and children were frequently sent out of state. One brash Chicago farmer even used the slogan: “It’s cheaper and easier to buy a baby for $100.00 than to have one of your own.”

Maternity homes and lying-in hospitals where doctors and midwives worked as for-profit adoption brokers were, like baby farms, an important part of the commercial adoption scene. Newspaper advertising was the primary technique they used to reach potential customers and suppliers. “For Adoption at Birth, Full Surrender, No Questions Asked.” In several cities, early adoption reformers investigated adoption ads. Campaigns to eradicate the marketing strategies common in commercial adoption were a primary means of eradicating black market adoptions themselves.

## Evidence
1. **Books**:
   - "The Baby Farmers: A Chilling Tale of Missing Children, Murder, and the Victorian Underworld" by Annie Cossins.
   - "Child Care and Child Welfare: Outlines for Study" by Addams, Wald, and Robinson.

### Online Resources
- [University of Oregon: Baby Farming](https://pages.uoregon.edu/adoption/topics/babyfarming.html)
