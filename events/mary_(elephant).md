# Mary the Elephant

Mary, a beloved elephant, was known for her incredible strength and gentle nature. Born in the early 20th century, she was captured from the wild and brought to a traveling circus. Her life in captivity was filled with hardships, from long hours of training to performing in front of crowds under the big top.

## Life in the Circus
- **Early Days**: As a young elephant, Mary was separated from her family and transported to America. She quickly became one of the star attractions due to her size and intelligence.
- **Performances**: Mary's acts included balancing on a ball, performing tricks, and interacting with the audience. Despite the applause and cheers, her days were marked by a lack of freedom and natural behavior.
- **Training and Treatment**: The training methods used were often harsh and cruel, focusing on obedience rather than care. Mary, like many circus animals, faced physical and emotional challenges due to this treatment.

## The Tragic Incident
- **The Day of the Incident**: On a fateful day in 1916, Mary was involved in an incident that led to a tragic turn of events. During a performance in a small town, Mary reacted violently, reportedly due to mistreatment or confusion. This reaction led to the death of a circus worker.
- **Public Outcry**: The incident caused a public outcry, and the town demanded justice. Unfortunately, the outcome was grim for Mary.

## Execution and Aftermath
- **Execution Details**: The elephant was hanged by the neck from a railcar-mounted industrial derrick between four o'clock and five o'clock that afternoon. The first attempt resulted in a snapped chain, causing Mary to fall and break her hip as dozens of children fled in terror. The severely wounded elephant died during a second attempt and was buried beside the tracks.
- **Medical Examination**: A veterinarian examined Mary after the hanging and determined that she had a severely infected tooth in the precise spot where Red Eldridge had prodded her.
- **Photographic Evidence**: The authenticity of a widely distributed (and heavily retouched) photo of her death was disputed years later by Argosy magazine.

## Impact and Legacy
- **Animal Rights Awareness**: The tragic story of Mary the Elephant brought attention to the treatment of circus animals and sparked early conversations about animal rights and welfare.
- **Circus Reforms**: Over the years, her story has been used as a case for the reform of animal treatment in entertainment, leading to better regulations and the eventual decline of animal acts in circuses.
- **Cultural Memory**: Mary’s story remains a poignant reminder of the importance of compassion and proper treatment of animals, influencing generations to advocate for animal rights.

## Evidence
- [WIKI - Mary the Elephant](https://en.wikipedia.org/wiki/Mary_(elephant))