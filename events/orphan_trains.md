# Orphan Trains

Starting in the 1850s, trains loaded with orphans traveled from the Eastern United States through the Midwest and into the West. The trains would stop in farming communities, and farmers who didn’t have children could easily adopt from the cargo of children. However, the children were more often used for labor.

The practice didn’t cease until the 1930s, when cheap agricultural labor was no longer in demand. It’s estimated that some 200,000 orphaned, homeless, and destitute children were dispatched in this manner, though documentation wasn’t precise.

## Impact and Legacy
- The Orphan Train Movement is considered the beginning of documented foster care in America.
- Many children faced harsh conditions and were used primarily for labor rather than being integrated into families.

## Evidence
- [WIKI - The Orphan Trains](https://en.wikipedia.org/wiki/Orphan_Train)