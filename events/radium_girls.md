# Radium Girls

The Radium Girls were female factory workers in the early 20th century who contracted radiation poisoning from painting watch dials with self-luminous paint containing radium. The term refers to several groups of women who worked at U.S. Radium Corporation and other companies, primarily during the 1910s and 1920s.

## Historical Context
In the early 1900s, radium was considered a miracle element and was used in various consumer products, including luminous paint for watch dials. The workers, mostly young women, were instructed to lick their brushes to give them a fine point, ingesting small amounts of radium each time. They were told that the paint was harmless.

## Impact and Legacy
The health effects of radium exposure were not immediately understood. Over time, many of these women began to suffer from serious health issues, including anemia, bone fractures, and necrosis of the jaw, a condition now known as "radium jaw." The plight of the Radium Girls led to significant changes in occupational health and safety standards.

Their struggle for justice resulted in a landmark legal case that established the right of workers to sue their employers for damages caused by unsafe working conditions. The Radium Girls' case was instrumental in the establishment of the Occupational Safety and Health Administration (OSHA) and the enactment of regulations to protect workers from industrial hazards.

## Evidence
### Secondary Sources
1. **Books**:
   - "The Radium Girls: The Dark Story of America’s Shining Women" by Kate Moore.
   - "Deadly Glow: The Radium Dial Worker Tragedy" by Ross Mullner.


### Online Resources
- [Wikipedia: Radium Girls](https://en.wikipedia.org/wiki/Radium_Girls)
