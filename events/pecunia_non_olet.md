# Pecunia Non Olet

**Pecunia non olet** is a Latin phrase that translates to "money does not stink." It refers to the idea that the value of money is not tainted by its origins. This phrase is famously attributed to the Roman Emperor Vespasian.

## Historical Context
In the 1st century AD, Emperor Vespasian imposed a tax on public urinals. When his son, Titus, criticized this source of revenue as disgusting, Vespasian held a coin from the tax proceeds to Titus's nose and asked whether the smell was offensive. Titus replied that it was not, leading Vespasian to declare, "Pecunia non olet."

## Evidence
- [Wikipedia: Pecunia Non Olet](https://en.m.wikipedia.org/wiki/Pecunia_non_olet)